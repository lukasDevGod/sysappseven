import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { ProdutosPage } from '../pages/produtos/produtos';
import { CarrinhoPage } from '../pages/carrinho/carrinho';
import { InventRioPage } from '../pages/invent-rio/invent-rio';
import { TabsControllerPage } from '../pages/tabs-controller/tabs-controller';
import { LoginPage } from '../pages/login/login';
import { HttpClientModule } from '@angular/common/http'; 
import { HttpModule } from '@angular/http';

import { DatePipe } from '@angular/common';
import { IonicStorageModule } from '@ionic/storage'
import { StatusBar } from '@ionic-native/status-bar';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BarcodePage } from '../pages/barcode/barcode';

import { ModalProductPage } from '../pages/modal-product/modal-product';
import { ProductStorageProvider } from '../providers/product-storage/product-storage';
import { EditProductPage } from '../pages/edit-product/edit-product';
import { ProductApiProvider } from '../providers/product-api/product-api';
import { GenericApiProvider } from '../providers/generic-api/generic-api';
import { BarcodePageModule } from '../pages/barcode/barcode.module';
//import { ApiService } from '../../src/providers/api-service/api-service'

@NgModule({
  declarations: [
    MyApp,
    ProdutosPage,
    CarrinhoPage,
    InventRioPage,
    TabsControllerPage,
    LoginPage,
    ModalProductPage,
    EditProductPage
    //ProductStorageProvider
  ],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(MyApp),
    HttpModule,HttpClientModule,
    IonicStorageModule.forRoot(),
    BarcodePageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ProdutosPage,
    CarrinhoPage,
    InventRioPage,
    TabsControllerPage,
    LoginPage,
    BarcodePage,
    ModalProductPage ,
    EditProductPage
   // ProductStorageProvider
  ],
  providers: [
    StatusBar,
    SplashScreen,BarcodeScanner,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HttpClientModule,
    ProductStorageProvider,
    DatePipe,
    ProductApiProvider,
    GenericApiProvider
  ]
})
export class AppModule {}
