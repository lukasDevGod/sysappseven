import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { ProdutosPage } from '../pages/produtos/produtos';
import { CarrinhoPage } from '../pages/carrinho/carrinho';


import { LoginPage } from '../pages/login/login';
import { BarcodePage } from '../pages/barcode/barcode';
import { TabsControllerPage } from '../pages/tabs-controller/tabs-controller';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) navCtrl: Nav;
    rootPage:any = LoginPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
  goToLogin(){
    this.navCtrl.setRoot(LoginPage);
  }
    goToProdutos(params){ 
    if (!params) params = {};
    this.navCtrl.setRoot(ProdutosPage);
  }goToCarrinho(params){
    if (!params) params = {};
    this.navCtrl.setRoot(CarrinhoPage);
  }

  goToCodeBarReader(){
    
    this.navCtrl.push(BarcodePage);
  }

}
