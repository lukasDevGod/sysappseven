import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ProdutosPage } from '../produtos/produtos';
import { CarrinhoPage } from '../carrinho/carrinho';
import { InventRioPage } from '../invent-rio/invent-rio';
import { BarcodePage } from '../barcode/barcode';

@Component({
  selector: 'page-tabs-controller',
  templateUrl: 'tabs-controller.html'
})
export class TabsControllerPage {

 // tab1Root: any = ProdutosPage;
  tab2Root: any = BarcodePage;
  tab3Root: any = CarrinhoPage;
  constructor(public navCtrl: NavController) {
  }
  
}
