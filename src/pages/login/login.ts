import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { ProdutosPage } from '../produtos/produtos';
import { CarrinhoPage } from '../carrinho/carrinho';
import { TabsControllerPage } from '../tabs-controller/tabs-controller';
import { GenericApiProvider } from '../../providers/generic-api/generic-api';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';



@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  usuario: string = ''
  senha: string = ''
  constructor(public navCtrl: NavController, private login: GenericApiProvider,private toast:ToastController, private http:Http) {
  }
  goToProdutos(params) {
    if (!params) params = {};
    this.navCtrl.push(ProdutosPage);
  } goToCarrinho(params) {
    if (!params) params = {};
    this.navCtrl.push(CarrinhoPage);
  }

  Login() {
    this.login.getLogin(this.usuario, this.senha)
      .then((result) => {
        if(result === 'true'){
          this.goToMain()
          this.toast.create({ message: 'Bem Vindo', duration: 1000, position: 'bottom' }).present();
        }
        else
          this.toast.create({ message: 'Login Invalido', duration: 2000, position: 'bottom' }).present();
      })
      .catch((err) => {
        this.toast.create({ message: 'Login Invalido', duration: 2000, position: 'bottom' }).present();
        console.log(err)
      })
  }


  goToMain() {
   
    this.navCtrl.setRoot(TabsControllerPage);
  }


}
