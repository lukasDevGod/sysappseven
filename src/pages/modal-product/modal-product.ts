import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { ProductStorageProvider, Produto } from '../../providers/product-storage/product-storage';
import { FormGroup } from '@angular/forms';
import { ProductApiProvider, Estoque } from '../../providers/product-api/product-api';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';

/**
 * Generated class for the ModalProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-product',
  templateUrl: 'modal-product.html',
})


export class ModalProductPage {
  model: Produto;
  estoque:Estoque
  key: string;
  qtdCompra: number = 0
  nomeProd: string;
  qtdEstoque: number
  idProduto:number

  dadosProdutos: any = []
  constructor(public http:Http,public apiProduto:ProductApiProvider,public navCtrl: NavController, public Toast: ToastController, public navParams: NavParams, private view: ViewController, private toast: ToastController, private produtoProvider: ProductStorageProvider) {
    if (this.navParams.data.produto && this.navParams.data.key) {
      this.model = this.navParams.data.produto;
      this.key = this.navParams.data.key;
    } else {
      this.model = new Produto();
    }

    this.estoque = new Estoque
  }

  ionViewWillLoad() {
    this.dadosProdutos = this.navParams.get('data')

    for(var i in this.dadosProdutos){
        this.qtdEstoque = this.dadosProdutos[i].quant_estoque
       

    }
  }

  closeModal() {
    this.view.dismiss();
  }

  save() {

    
    if (this.formValidator()) {
      this.saveContact()
    }

  }



  public saveContact() {
    if (this.key) {

      this.navCtrl.pop()
      this.Toast.create({ message: 'Produto Adicionado ao Carrinho.', duration: 2000, position: 'botton' }).present();



      console.log(this.model)
      return this.produtoProvider.update(this.key, this.model)

    } else {


      this.navCtrl.pop()
      this.Toast.create({ message: 'Produto Adicionado ao Carrinho.', duration: 2000, position: 'botton' }).present();

      
      for (var i in this.dadosProdutos) {
        this.model.id = this.dadosProdutos[i].id
        this.model.name = this.dadosProdutos[i].nome
        this.model.preco = this.dadosProdutos[i].preco_venda 
        this.idProduto = this.dadosProdutos[i].id
        
      }
      this.model.cpf = '0'
      this.model.total = this.sumTotal(this.model.preco,this.qtdCompra)
      this.model.qtd = this.qtdCompra;
      let sobra = this.qtdEstoque - this.qtdCompra
      
      this.estoque.id = this.idProduto
      this.estoque.qtd = sobra
      
      
     this.AtualizaEstoque(this.estoque)
     
      return this.produtoProvider.insert(this.model);

    }

  }

  formValidator() {
    
    if ((this.qtdCompra > this.qtdEstoque) && (this.qtdCompra > 0) )  {
      this.Toast.create({ message: 'Quantidade de Compra maior que quantidade de Estoque', duration: 2000, position: 'botton' }).present()
      return false
    } else {

      return true
    }
  }

  sumTotal(vlr:number, qtd:number){
    let sum = Number(vlr) * Number(qtd)
    return sum
  }

  
  AtualizaEstoque(estoque:Estoque){
   return this.apiProduto.AtualizaEstoque(estoque)
      
  }



}

