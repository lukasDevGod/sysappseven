import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { Produto, ProductStorageProvider } from '../../providers/product-storage/product-storage';
import { ProductApiProvider, Estoque } from '../../providers/product-api/product-api';

/**
 * Generated class for the EditProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-product',
  templateUrl: 'edit-product.html',
})
export class EditProductPage {
  model: Produto;
  key: string;
  qtd: number = 0
  qtdInicial: number = 0
  qtdTotal: number = 0
  estoque: Estoque
  data = {}

  dadosProdutos: any = []
  constructor(public apiProduto: ProductApiProvider, public navCtrl: NavController, public navParams: NavParams, private view: ViewController, private toast: ToastController, private produtoProvider: ProductStorageProvider) {
    if (this.navParams.data.produto && this.navParams.data.key) {
      this.model = this.navParams.data.produto;
      this.key = this.navParams.data.key;
    } else {
      this.model = new Produto();
    }
    this.estoque = new Estoque
  }

  ionViewWillLoad() {
    this.dadosProdutos = this.navParams.get('data')
    //console.log(this.dadosProdutos)
    console.log(this.model)
    this.qtdInicial = Number(this.model.qtd)



  }

  save() {
    // if (this.verificaEstoque(this.model.id)) {
    this.saveContact()
    // }
  }

  public updateEstoque(id: number) {


    this.apiProduto.getProduto(id)
      .then((value) => {
        this.data = value
        let qtd_estoque = this.data[0]['quant_estoque']
        let valor = Number(this.model.qtd) - Number(this.qtdInicial)

        let total = Number(qtd_estoque) - Number(valor)

        this.estoque.id = id
        this.estoque.qtd = total
        this.apiProduto.AtualizaEstoque(this.estoque)
      })


  }

  public saveContact() {
    if (this.key) {
      this.navCtrl.pop()

      this.updateEstoque(this.model.id)
      this.model.total = this.sumTotal(this.model.preco, this.model.qtd)
      return this.produtoProvider.update(this.key, this.model)
    } else {
      this.navCtrl.pop()
      this.model.total = this.sumTotal(this.model.preco, this.model.qtd)
      return this.produtoProvider.insert(this.model);
    }

  }

  sumTotal(vlr: number, qtd: number) {
    let sum = Number(vlr) * Number(qtd)
    return sum
  }

  verificaEstoque(id: number) {
    this.apiProduto.getProduto(id)
      .then((res) => {
        this.qtd = Number(res['0']['qtd'])
        this.qtdTotal = (this.qtd + this.qtdInicial)

        if (this.model.qtd > this.qtdTotal) {
          this.toast.create({ message: 'Quantidade Indisponivel.', duration: 3000, position: 'botton' }).present();

          return false

        } else {

          return true
        }
      })




  }

}
