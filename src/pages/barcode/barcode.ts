import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, ToastController } from 'ionic-angular';
import { BarcodeScannerOptions, BarcodeScanner } from '@ionic-native/barcode-scanner';
import { ModalProductPage } from '../modal-product/modal-product';
import { ProductApiProvider} from '../../providers/product-api/product-api';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

/**
 * Generated class for the BarcodePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-barcode',
  templateUrl: 'barcode.html',
})
export class BarcodePage {
  options: BarcodeScannerOptions
  encodeText: String = ''
  encodeData: any = {}
  scannedData: any = {}
  scanNumb: number = 5
  nomeProd: string = ''
  myData = {}


  constructor(public Toast: ToastController, public productProv: ProductApiProvider, public navCtrl: NavController, public navParams: NavParams, public scanner: BarcodeScanner, private modal: ModalController) {
    
  }

  scan() {
    this.options = {

      prompt: 'Escaneie seu código de barras'
    }
    this.scanner.scan().then((data) => {
      

      
      this.scannedData = data
      if (this.scannedData.text > 0){ 
      this.getProduto(this.scannedData.text)
    }
    }, (err) => {
      console.log('Error', err)
    })
  }

  encode() {
    this.scanner.encode(this.scanner.Encode.TEXT_TYPE, this.encodeText).then((data) => {
      this.encodeData = data

    }, (err) => {

      console.log('Error', err)
    })
  }

  openModal() {
    const myModal = this.modal.create(ModalProductPage, { data: this.myData })
    myModal.present()

  }

  getProduto(id: number) {
    this.productProv.getProduto(id)
      .then((result) => {
        this.myData = result

        this.openModal()


      })
      .catch((err)=>{
        console.log(err)
      })
  }

  getProdutoName(name: string) {
    this.productProv.getProdutoNome(name)
      .then((result) => {
        this.myData = result
        if (this.myData != '')
          this.openModal()
        else
          this.Toast.create({ message: 'Digite corretamente o nome do Produto', duration: 3000, position: 'botton' }).present();
      })
      .catch((err)=>{
        console.log(err)
      })
     
  }

  PesquisaNome() {
    this.getProdutoName(this.nomeProd)

  }

  get() {
    this.getProduto(this.scannedData)
  }

}
