import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { CarrinhoPage } from '../carrinho/carrinho';
//import { ProdutosPage } from '../produtos/produtos';
//import { ApiService } from '../../providers/api-service/api-service'
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Component({
  selector: 'page-produtos',
  templateUrl: 'produtos.html'
})
export class ProdutosPage {
  // public obj: String;
  // public result: ApiService

  products;
  groupedProducts = [];
  dadosProdutos: { quant: number, name: String }[] = []
  currentNumber = 0

  constructor(public navCtrl: NavController, public http: Http, public alertCtrl: AlertController) {

  }

  ionViewWillLoad(){
    
  }


  goToCarrinho() {

    this.navCtrl.push(CarrinhoPage, this.dadosProdutos);


  }


  goToProdutos(params) {
    if (!params) params = {};
    this.navCtrl.push(ProdutosPage);
  }
}
