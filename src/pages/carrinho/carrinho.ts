import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { ProdutosPage } from '../produtos/produtos';
//import { CarrinhoPage } from '../carrinho/carrinho';
import { ProductStorageProvider, Produto, ProductList } from '../../providers/product-storage/product-storage';
import { ModalProductPage } from '../modal-product/modal-product';
import { EditProductPage } from '../edit-product/edit-product';
import { ProductApiProvider, Estoque } from '../../providers/product-api/product-api';


@Component({
  selector: 'page-carrinho',
  templateUrl: 'carrinho.html'
})
export class CarrinhoPage {
  dadosProdutos = []
  items: Array<string> = new Array<string>();
  produtos: ProductList[]
  p:ProductList
  data = {}
  estoque:Estoque
  constructor(public alertCtrl: AlertController, public navCtrl: NavController, public navParam: NavParams, private Toast: ToastController, public productProvider: ProductStorageProvider, public api: ProductApiProvider) {


    this.estoque = new Estoque
    //this.dadosProdutos = this.navParam.data
    //console.log(this.dadosProdutos)

  }

  ionViewDidEnter() {
    this.productProvider.getAll()
    .then(results => {
      this.produtos = results
      for (let index = 0; index < this.produtos.length; index++) {

        this.items[index] = JSON.stringify(this.produtos[index]['produto'])
        
      }

    })

  }

  carregaArrayProduto(){
      for (let index = 0; index < this.produtos.length; index++) {

        this.items[index] = JSON.stringify(this.produtos[index]['produto']) 
        this.api.InsereDados(this.items[index])
    }

/*     this.items.forEach(element => {
      this.api.InsereDados(element)
     }) */
  }

  clear() {
    this.productProvider.clear()
    this.navCtrl.setRoot(this.navCtrl.getActive().component);
  }

  editProduct(item: ProductList) {
    this.navCtrl.push(EditProductPage, { key: item.key, produto: item.produto });
  }

  removeProduct(item: ProductList) {
    this.api.getProduto(item.produto.id)
    .then((result)=>{     
     this.data = result     
     
     let qtd_estoque = this.data[0]['quant_estoque']
     let qtd = item.produto.qtd
     let total = Number(qtd_estoque) + Number(qtd)

     this.estoque.id = item.produto.id
     this.estoque.qtd = total
    
     this.api.AtualizaEstoque(this.estoque)

    })
    this.productProvider.remove(item.key)
      .then(() => {
        let index = this.produtos.indexOf(item)
        this.produtos.splice(index, 1)

        this.Toast.create({ message: 'Produto Removido.', duration: 3000, position: 'botton' }).present();
      })
  }


  goToProdutos() {

    this.navCtrl.push(ProdutosPage);


  }

  goToCarrinho(params) {
    if (!params) params = {};
    this.navCtrl.push(CarrinhoPage);
  }


  FinalizaVenda(cpf:string) {
    for (var i in this.produtos){
      this.produtos[i].produto.cpf = cpf   
    }
    this.carregaArrayProduto()
       

  
    this.clear()
    this.Toast.create({ message: 'Venda finalizada com sucesso.', duration: 2000, position: 'botton' }).present();

  }

  addParamJSON(cpf:string){
    console.log(this.items)
    this.items['cpf'] = cpf
   
   
   /*  console.log(this.items)
    console.log(this.produtos ) */
  }

  showConfirm() {
    const confirm = this.alertCtrl.create({
      title: 'Finalizar Venda',
      message: 'Deseja finalizar sua Venda ?',
      inputs: [
        {
          name: 'CPF',
          placeholder: 'Digite o CPF do cliente'
        },
      ],

      buttons: [
        {
          text: 'Não',
          handler: (data) => {

          }
        },
        {
          text: 'Sim',
          handler: (data) => {
            if (data['CPF'] != '') {             
              //this.addParamJSON(data['CPF'])
              this.FinalizaVenda(data['CPF'])

            } else {

              this.Toast.create({ message: 'Digite o CPF do cliente.', duration: 3000, position: 'botton' }).present();
              return false

            }
          }
        }
      ]
    });
    confirm.present();
  }
}

