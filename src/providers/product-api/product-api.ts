import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Produto } from '../product-storage/product-storage';


/*
  Generated class for the ProductApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProductApiProvider {
  headers: Headers;
  options: RequestOptions;
  API_URL = 'http://localhost:8080/produto/'
  AP = 'https://reqres.in/api/'
  constructor(public http: Http) {

     this.headers = new Headers();
     this.headers.set('Access-Control-Allow-Origin', '*')
     this.options = new RequestOptions({ headers: this.headers });


  }

  getProduto(id: number) {
    return new Promise((resolve, reject) => {

      let url = this.API_URL + id;

      this.http.get(url)
        .subscribe((result: any) => {
          resolve(result.json());
        },
          (error) => {
            reject(error.json());
          }
        )

    })
  }




  getProdutoNome(name: string) {
    return new Promise((resolve, reject) => {

      let url = this.API_URL + 'nomeProd/' + name;

      this.http.get(url)
        .subscribe((result: any) => {
          resolve(result.json());
        },
          (error) => {
            reject(error.json());
          }
        )

    })
  }

  
/*   putAtualizaEstoque(estoque: Estoque) {
    let body = estoque
  
    //let body = JSON.parse(str)
    let url = this.API_URL + 'estoque/' + encodeURIComponent(JSON.stringify(body))
    console.log(url)
    console.log(body)
    
    
    return this.http
      .post(encodeURIComponent(url), body)
      .subscribe(res => {
        console.log(res.text())

      },
      (error) => {
        console.log(error.json());
      }
    );



  } */

  AtualizaEstoque(estoque:Estoque) {
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    headers.append('Access-Control-Allow-Origin', '*')
    const requestOptions = new RequestOptions({ headers: headers });
   

     let t =  JSON.stringify(estoque)

    this.http.post(this.API_URL +  'estoque/' + t + '' , requestOptions)
      .subscribe(data => {
        console.log(data['_body']);
       }, error => {
        console.log(error);
      });
  }


  InsereDados(produto:string) {
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    headers.append('Access-Control-Allow-Origin', '*')
    const requestOptions = new RequestOptions({ headers: headers });
   
   
    this.http.post(this.API_URL +  'insert/' + produto + '', {"title": "something"} , requestOptions)
      .subscribe(data => {
        console.log(data['_body']);
       }, error => {
        console.log(error);
      });
  }


}

export class Estoque {
  id: number;
  qtd: number;
}