import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';

/*
  Generated class for the GenericApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GenericApiProvider {
  API_URL = 'http://localhost:8080/'
  constructor(public http: Http) {
   
  }

  getLogin(usuario:string,senha:string) {
    return new Promise((resolve, reject) => {

      let url = this.API_URL + 'login/' + usuario + '/' + senha;

      this.http.get(url)
        .subscribe((result: any) => {
          resolve(result.json());
        },
          (error) => {
            reject(error.json());
          }
        )

    })
  }

}
