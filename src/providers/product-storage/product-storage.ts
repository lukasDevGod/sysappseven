
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage'
import { DatePipe } from '@angular/common'


@Injectable()
export class ProductStorageProvider {

  constructor(private storage: Storage, private datePipe: DatePipe) {
  
  }

public insert(produto:Produto){
   let key = this.datePipe.transform(new Date(),"ddMMyyyHHmmss")
   return this.save(key,produto)
}

public update(key:string,produto:Produto){
  return this.save(key,produto)
}

private save (key:string,produto:Produto){
  this.storage.set(key,produto)
}

public remove(key:string){
  return this.storage.remove(key)
  
}

public getAll(){
  let produtos: ProductList[]=[]
  
 return this.storage.forEach((value:Produto,key:string,iterationNumber:number)=>{
    let produto = new ProductList()
    produto.key = key;
    produto.produto = value; 
    produtos.push(produto)

  })
  .then(()=>{
    return Promise.resolve(produtos)
  })
  .catch((error)=>{
    return Promise.reject(error)
  })
}

public clear(){
  this.storage.clear()
}

}
export class Cliente{
  CPF:string
}

export class Produto{
  id:number
  name: string;
  preco:number
  qtd:number
  total:number
  cpf:string;

}

export class ProductList{
  key:string;
  produto:Produto;


}
